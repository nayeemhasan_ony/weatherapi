<?php
echo "test3";

?>

<html>

    <head>
         <title>Weather Details</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="script.js"></script>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
        <p id="test"><?php include 'weather.php';?></p>

        <div class="outputHolder" id="outputHolder">
            <div class="output" id="output">
                <h3 class="outputData"><?php echo $cityDetails?></h1>
                <table class="tab1">
                    
                        
                    <tr >
                        <td id="maxT" class="outputData">Max Temp</td>
                        <td class="outputData">: <?php echo $maxTemp?></td>
                    </tr>
                    
                    <tr >
                        <td id="minT" class="outputData">Min Temp</td>
                        <td class="outputData">: <?php echo $minTemp?></td>
                    </tr>
                    
                    <tr >
                        <td id="humidity" class="outputData">Humidity</td>                              
                        <td class="outputData">: <?php echo $humidity?></td>
                    </tr>
                    
                    <tr >
                        <td id="sunrise" class="outputData">Sunrise</td>
                        <td class="outputData">: <?php echo $sunrise?></td>
                    </tr>
                    
                    <tr >
                        <td id="sunset" class="outputData">Sunset</td>
                        <td class="outputData">: <?php echo $sunset?></td>
                    </tr>
                        
                </table>
                
                
         

            </div>
        </div>
        
        <div class="divMain">

    <form method="post" action="output.php"  onsubmit="return checkEmpty()">

        <div id="inputItems">
            
            <input type="text" id="cityID" name="city" placeholder="Name of the city...">          
            <button type="submit" id="but1" name="but1">Submit</button>

        </div>
        <p id="errorE" ></p>

    </form>
        </div>>
        </body>
    
    </html>